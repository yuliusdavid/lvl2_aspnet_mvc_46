﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
//using LVL2_ASPNet_MVC_46.Models;

namespace LVL2_ASPNet_MVC_46.Controllers
{
    public class ResponseMessage
    {
        private HttpStatusCode statusCode;
        private string statusMessage;
        private object data;

        public ResponseMessage(HttpStatusCode statusCode, string statusMessage, object data)
        {
            this.statusCode = statusCode;
            this.statusMessage = statusMessage;
            this.data = data;
        }

        public ResponseMessage(HttpStatusCode statusCode, string statusMessage)
        {
            this.statusCode = statusCode;
            this.statusMessage = statusMessage;
            this.data = null;
        }

        public ResponseMessage()
        {
            this.statusCode = HttpStatusCode.OK;
            this.statusMessage = "Success";
        }

        public HttpStatusCode StatusCode { get => statusCode; set => statusCode = value; }
        public string StatusMessage { get => statusMessage; set => statusMessage = value; }
        public object Data { get => data; set => data = value; }
    }

    public class EmployeeController : ApiController
    {
        //Declare obj entities
        private EmployeeContext db = new EmployeeContext();

        // GET: api/Employee
        [Authorize]
        public IHttpActionResult Get()
        {
            try
            {
                List<ms_employee> listEmployee = db.ms_employee.ToList();

                return Ok(new ResponseMessage(HttpStatusCode.OK, "Success", listEmployee));
            }
            catch (Exception ex)
            {
                return Ok(new ResponseMessage(HttpStatusCode.BadRequest, ex.Message));
            }

        }

        // GET: api/Employee/5
        [Authorize]
        public IHttpActionResult Get(int id)
        {
            try
            {
                ms_employee objEmployee = db.ms_employee.Find(id);

                return Ok(new ResponseMessage(HttpStatusCode.OK, "Success", objEmployee));
            }
            catch (Exception ex)
            {
                return Ok(new ResponseMessage(HttpStatusCode.BadRequest, ex.Message));
            }

        }

        // POST: api/Employee
        [Authorize]
        public IHttpActionResult Post([FromBody]ms_employee value)
        {
            try
            {
                db.ms_employee.Add(value);
                db.SaveChanges();

                return Ok(new ResponseMessage());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return Ok(new ResponseMessage(HttpStatusCode.BadRequest, ex.Message));
            }
        }

        // PUT: api/Employee
        [Authorize]
        public IHttpActionResult Put([FromBody]ms_employee value)
        {
            try
            {
                db.Entry(value).State = System.Data.Entity.EntityState.Modified;

                db.SaveChanges();
                return Ok(new ResponseMessage());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return Ok(new ResponseMessage(HttpStatusCode.BadRequest, ex.Message));
            }
        }

        // DELETE: api/Employee/5
        [Authorize]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                ms_employee objEmployee = db.ms_employee.Find(id);
                db.Entry(objEmployee).State = System.Data.Entity.EntityState.Deleted;

                db.SaveChanges();
                return Ok(new ResponseMessage());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return Ok(new ResponseMessage(HttpStatusCode.BadRequest, ex.Message));
            }
        }
       
    }
}